﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinqTask.Services;

namespace LinqTask
{
    class Program
    {
        static void Main()
        {
            
            QueryNumbers.EvenQuery();
            CustomerBalance.CustomerDetails();
            GroupJoin.JoinGroup();
            InnerJoin.JoinInner();
        }
    }
}
