﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqTask.Services
{
    public class Course
    {
        public int ID { get; set; }
        public string Name { get; set; }

        public static List<Course> GetAllCourses()
        {
            return new List<Course>()
            {
                new Course {ID = 1, Name = "Pure Science"},
                new Course {ID = 2, Name = "Environmental Management"},
                new Course {ID = 3, Name = "Engineering"},
                new Course {ID = 4, Name = "Business Management"}
            };
        }
            
    }
}
