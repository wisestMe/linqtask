﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqTask.Services
{
    public class Student
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public int CourseID { get; set; }
        public string Email { get; set; }

        public static List<Student> GetAllStudents()
        {
            return new List<Student>()
        {
            new Student {ID = 1, Name = "Marvellous", CourseID = 1, Email = "marvel@gmail.com"},
            new Student {ID = 2, Name = "Tochukwu", CourseID = 3, Email = "tiktok@gmail.com"},
            new Student {ID = 3, Name = "Stanley", CourseID = 4, Email = "leestan@yahoo.com"},
            new Student {ID = 4, Name = "Idam", CourseID = 2, Email = "idam@hotmail.com"},
            new Student {ID = 5, Name = "Emeka", CourseID = 1, Email = "mekus@yahoo.com"},
            new Student {ID = 6, Name = "Stephanie", CourseID = 3, Email = "stephnol@gmail.com"},
            new Student {ID = 7, Name = "Joseph", CourseID = 4, Email = "jojo@gmail.com"},
            new Student {ID = 8, Name = "Kezy", CourseID = 2, Email = "kezman@hotmail.com"},
            new Student {ID = 9, Name = "Chidiebube", CourseID = 2, Email = "chibube@yahoo.com"},
            new Student {ID = 10, Name = "Joshua", CourseID = 3, Email = "talk2josh@gmail.com"}
        };
        }
    }

    
}
