﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqTask.Services
{
    class InnerJoin
    {
        public static void JoinInner()
        {
            Console.WriteLine("Implement Inner Join \n ---------------------------");
            var result = Student.GetAllStudents().Join(Course.GetAllCourses(),
                                                   s => s.CourseID,
                                                    c => c.ID,
                                                    (student, course) => new
                                                    {
                                                        StudentName = student.Name,
                                                        CourseName = course.Name
                                                    });
            foreach (var student in result)
            {
                Console.WriteLine(student.StudentName + "\t" + student.CourseName);
            }
        }
    }
}
