﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqTask.Services
{
    class GroupJoin
    {
        public static void JoinGroup()
        {
            Console.WriteLine("-------------------- \n Group Join Implementation \n --------------------");
            var studentsByCourse = LinqTask.Services.Course.GetAllCourses()
                                                            .GroupJoin(Student.GetAllStudents(),
                                                                        c => c.ID,
                                                                        s => s.CourseID,
                                                                        (course, students) => new
                                                                        {
                                                                            Course = course,
                                                                            Students = students
                                                                        });
            foreach (var course in studentsByCourse)
            {
                Console.WriteLine(course.Course.Name);
                foreach (var student in course.Students)
                {
                    Console.WriteLine(" " + student.Name);
                }
                Console.WriteLine();
            }
        }
    }
}
