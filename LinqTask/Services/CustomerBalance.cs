﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinqTask.Services
{
    public class CustomerBalance
    {
        
        string name, phone, address;
        int balance;
        public CustomerBalance(string Name, string Phone, string Address, int Balance)
        {
            this.name = Name;
            this.phone = Phone;
            this.address = Address;
            this.balance = Balance;
        }

        
        public static void CustomerDetails()
        {
            List<CustomerBalance> customers = new List<CustomerBalance>();
            customers.Add(new CustomerBalance("Tochukwu", "0803452654", "Tochukwu street", 500));
            customers.Add(new CustomerBalance("Marvellous", "080387654", "Marvellous drive", 800));
            customers.Add(new CustomerBalance("Stanley", "08037676", "Tochukwu street", 300));
            customers.Add(new CustomerBalance("Emeka", "08039879", "Tochukwu street", 250));
            customers.Add(new CustomerBalance("Stephanie", "0803123687", "Stephanie Close", -100));

            var overdue =
           from cust in customers
           where cust.balance < 0
           orderby cust.balance ascending
           select new { cust.name, cust.balance };

            foreach (var cust in overdue)
            {
                Console.WriteLine("Name = {0} and Balance = {1}", cust.name, cust.balance);
            }

        }

       

    }
}
